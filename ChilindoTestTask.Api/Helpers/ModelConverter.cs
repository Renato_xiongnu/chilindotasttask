﻿using ChilindoTestTask.DTO;
using ChilindoTestTask.Model;
using System.Collections.Generic;

namespace ChilindoTestTask.Api.Helpers
{
    public static class ModelConverter
    {
        /// <summary>
        /// ToTransactionModel
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Transaction ToTransactionModel(this TransactionDTO obj)
        {
            return new Transaction
            {                                 
                AccountId = obj.AccountId,
                Amount = obj.Amount,
                Currency = obj.Currency
            };
        }


        /// <summary>
        /// ToTransactionModelList
        /// </summary>
        /// <param name="listObj"></param>
        /// <returns></returns>
        public static IList<Transaction> ToTransactionModelList(this List<TransactionDTO> listObj)
        {
            var list = new List<Transaction>();

            foreach (var item in listObj)
            {
                list.Add(item.ToTransactionModel());
            }

            return list;
        }


    }//class
}