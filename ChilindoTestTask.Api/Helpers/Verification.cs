﻿using ChilindoTestTask.DTO;

namespace ChilindoTestTask.Api.Helpers
{
    public static class Verification
    {
        /// <summary>
        /// IsTransactionValid
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public static bool IsTransactionValid(this TransactionDTO trans)
        {
            return (trans.AccountNumber > 0 && trans.Amount > 0);
        }


    }//class
}