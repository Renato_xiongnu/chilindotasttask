﻿using ChilindoTestTask.DTO;
using System.ServiceModel;
using System.ServiceModel.Web;


namespace ChilindoTestTask.Api
{
    [ServiceContract]
    public interface ITransactionSvc
    {
        /// <summary>
        /// Balance
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "/Balance/{accountNumber}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        ServerResponse Balance(string accountNumber);


        /// <summary>
        /// Deposit
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        [WebInvoke(Method = "PUT", UriTemplate = "/Deposit", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        ServerResponse Deposit(TransactionDTO trans);


        /// <summary>
        /// Withdraw
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        [WebInvoke(Method = "PUT", UriTemplate = "/Withdraw", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        ServerResponse Withdraw(TransactionDTO trans);


    }//interface
}
