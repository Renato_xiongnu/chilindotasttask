﻿using ChilindoTestTask.Api.Helpers;
using ChilindoTestTask.DAL;
using ChilindoTestTask.DAL.Interfaces;
using ChilindoTestTask.DTO;
using ChilindoTestTask.Model;
using Microsoft.Practices.Unity;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;

namespace ChilindoTestTask.Api
{
    public class TransactionSvc : ITransactionSvc
    {
        private IUnityContainer _container;

        public TransactionSvc()
        {
            _container = new UnityContainer();

            _container.RegisterType<IUnitOfWork, UnitOfWork>();
        }

        public TransactionSvc(IUnityContainer container)
        {
            _container = container;
        }


        public IUnitOfWork DalSvc
        {
            get {
                return _container.Resolve<IUnitOfWork>();
            }
        }


        /// <summary>
        /// Balance
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        public ServerResponse Balance(string accountNumber)
        {
            int accNumber;

            if (!int.TryParse(accountNumber, out accNumber))
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            if (accNumber <= 0)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            var acc = DalSvc.AccountRepository.Filter(p => p.AccountNumber == accNumber).AsNoTracking().FirstOrDefault();

            //no such the account
            if (acc == null)
            {
                return new ServerResponse
                            {
                                AccountNumber = accNumber,
                                Currency = string.Empty,
                                IsSuccessful = false,
                                Message = "Such the account does not exist"
                            };
            }

            var trans = DalSvc.TransRepository.Filter(p => p.Account.AccountNumber == accNumber).ToList();
            decimal balance = trans.Select(p => p.Amount).Sum();


            return new ServerResponse
                        {
                            AccountNumber = accNumber,
                            Balance = balance,
                            Currency = trans.Count >0 ? trans.First().Currency : string.Empty,
                            IsSuccessful = true,
                            Message = string.Empty
                        };
        }


        /// <summary>
        ///Deposit
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public ServerResponse Deposit(TransactionDTO trans)
        {
            if (trans.IsTransactionValid())
            {
                using (var dbTrans = DalSvc.BeginTransaction())
                {
                    var acc = DalSvc.AccountRepository.Filter(p=>p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();

                    if (acc == null)
                    {
                        var accNew = new Account {
                             AccountNumber = trans.AccountNumber
                        };
                        bool accRes = DalSvc.AccountRepository.Insert(accNew);

                        if (!accRes)
                        {
                            throw new WebFaultException(HttpStatusCode.InternalServerError);
                        }
                        else
                        {
                            acc = DalSvc.AccountRepository.Filter(p => p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();
                        }
                    }

                    var transModel = trans.ToTransactionModel();
                    transModel.AccountId = acc.AccountId;


                    bool res = DalSvc.TransRepository.Insert(transModel);


                    decimal balance = DalSvc.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();
                    dbTrans.Commit();


                    return new ServerResponse
                                        {
                                            AccountNumber = acc.AccountNumber,
                                            Balance = balance,
                                            Currency = trans.Currency,
                                            IsSuccessful = res,
                                            Message = string.Empty
                                        };
                }//dbTrans
            }
            else
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }



        /// <summary>
        /// Withdraw
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        public ServerResponse Withdraw(TransactionDTO trans)
        {
            if (trans.IsTransactionValid())
            {
                using (var dbTrans = DalSvc.BeginTransaction())
                {
                    var acc = DalSvc.AccountRepository.Filter(p => p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();

                    //No such the account
                    if (acc == null)
                    {
                        return new ServerResponse
                        {
                            AccountNumber = acc.AccountNumber,
                            Currency = string.Empty,
                            IsSuccessful = false,
                            Message = "Such the account does not exist"
                        };
                    }

                    decimal balance = DalSvc.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();

                    if (trans.Amount > balance)
                    {
                        return new ServerResponse
                        {
                            AccountNumber = acc.AccountNumber,
                            Currency = trans.Currency,
                            IsSuccessful = false,
                            Message = "Insufficient funds"
                        };
                    }

                    //trans is ok, add a new one with a negative sign
                    trans.Amount *= -1;
                    trans.AccountId = acc.AccountId;
                    var res = DalSvc.TransRepository.Insert(trans.ToTransactionModel());


                    balance = DalSvc.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();
                    dbTrans.Commit();

                    return new ServerResponse
                                {
                                    AccountNumber = acc.AccountNumber,
                                    Balance = balance,
                                    Currency = trans.Currency,
                                    IsSuccessful = res,
                                    Message = string.Empty
                                };

                }//dbTrans
            }
            else
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

    }//class
}
