﻿using ChilindoTestTask.DTO;
using RestSharp;
using System;
using System.Configuration;
using System.Web.Script.Serialization;

namespace ChilindoTestTask.Client
{
    class Program
    {
        //Note: this is a simple Rest client, so the methods here are not optimized! This is a demo only!
        static void Main(string[] args)
        {
            bool isLoop = true;
            
            string svcurl_wcfrest = ConfigurationManager.AppSettings["WcfRestUrl"];
            string svcurl_webapi = ConfigurationManager.AppSettings["WebApiUrl"];

            do
            {
                Console.WriteLine(@"
                    - Account management -
                        1. Balance
                        2. Deposit
                        3. Withdraw
                        Esc to exit
                    Write number option: ");

                var consoleKey = Console.ReadKey().Key;

                switch (consoleKey)
                {
                    case ConsoleKey.D1:
                        Console.WriteLine("Enter the account number: ");
                        var accStr = Console.ReadLine();

                        int accNum = 0;

                        if (int.TryParse(accStr, out accNum))
                        {
                            var resp = RequestBalance(svcurl_wcfrest, accNum);

                            PrintResponse(resp);
                        }
                        break;
                    case ConsoleKey.D2:
                        Console.WriteLine("Enter the account number");
                        var depAccStr = Console.ReadLine();
                        Console.WriteLine("Enter amount");
                        var depAmountStr = Console.ReadLine();
                        Console.WriteLine("Enter currency");
                        var depCurStr = Console.ReadLine();

                        int depAccNum = 0;
                        decimal depAmount = 0;

                        if (int.TryParse(depAccStr, out depAccNum) && decimal.TryParse(depAmountStr, out depAmount))
                        {
                            var trans = new TransactionDTO {
                                 AccountNumber = depAccNum,
                                 Amount = depAmount,
                                 Currency = depCurStr
                            };

                            var resp = Deposit(svcurl_wcfrest, trans);
                            PrintResponse(resp);
                        }

                        break;
                    case ConsoleKey.D3:
                        Console.WriteLine("Enter the account number");
                        var withAccStr = Console.ReadLine();
                        Console.WriteLine("Enter amount");
                        var withAmountStr = Console.ReadLine();
                        Console.WriteLine("Enter currency");
                        var withCurStr = Console.ReadLine();

                        int withAccNum = 0;
                        decimal withAmount = 0;

                        if (int.TryParse(withAccStr, out withAccNum) && decimal.TryParse(withAmountStr, out withAmount))
                        {
                            var trans = new TransactionDTO
                            {
                                AccountNumber = withAccNum,
                                Amount = withAmount,
                                Currency = withCurStr
                            };

                            var resp = Withdraw(svcurl_wcfrest, trans);
                            PrintResponse(resp);
                        }

                        break;

                    case ConsoleKey.Escape:
                        isLoop = false;
                        break;
                }

            } while (isLoop);

    }//main

        #region "Private Methods"

        /// <summary>
        /// RequestBalance
        /// </summary>
        /// <param name="url"></param>
        /// <param name="accNumber"></param>
        /// <returns></returns>
        private static ServerResponse RequestBalance(string url, int accNumber)
        {
            var client = new RestClient(url);     
            var request = new RestRequest($"Balance/{accNumber}", Method.GET);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();

            var response = client.Execute(request);
            var json = response.Content;
            var obj = new JavaScriptSerializer().Deserialize<ServerResponse>(json);

            return obj;
        }

        /// <summary>
        /// Deposit
        /// </summary>
        /// <param name="url"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        private static ServerResponse Deposit(string url, TransactionDTO trans)
        {
            var client = new RestClient(url);
            var request = new RestRequest("Deposit", Method.PUT);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            string transJson = new JavaScriptSerializer().Serialize(trans);
            request.AddParameter("application/json", transJson, ParameterType.RequestBody);

            var response = client.Execute(request);
            var json = response.Content;
            var obj = new JavaScriptSerializer().Deserialize<ServerResponse>(json);

            return obj;
        }

        /// <summary>
        /// Withdraw
        /// </summary>
        /// <param name="url"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        private static ServerResponse Withdraw(string url, TransactionDTO trans)
        {
            var client = new RestClient(url);
            var request = new RestRequest("Withdraw", Method.PUT);
            request.AddHeader("Accept", "application/json");
            request.Parameters.Clear();
            string transJson = new JavaScriptSerializer().Serialize(trans);
            request.AddParameter("application/json", transJson, ParameterType.RequestBody);

            var response = client.Execute(request);
            var json = response.Content;
            var obj = new JavaScriptSerializer().Deserialize<ServerResponse>(json);

            return obj;
        }

        /// <summary>
        /// PrintResponse
        /// </summary>
        /// <param name="obj"></param>
        private static void PrintResponse(ServerResponse obj)
        {
            Console.WriteLine("Response:");
            Console.WriteLine($"Account number: {obj.AccountNumber}");
            Console.WriteLine($"Balance: {obj.Balance}");
            Console.WriteLine($"Currency: {obj.Currency}");
            Console.WriteLine($"IsSuccessful: {obj.IsSuccessful}");
            Console.WriteLine($"Message: {obj.Message}");
            Console.WriteLine("---------------------");
        }

        #endregion

    }//class
}
