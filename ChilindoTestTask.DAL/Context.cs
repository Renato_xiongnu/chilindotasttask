﻿using ChilindoTestTask.Model;
using System.Data.Entity;

namespace ChilindoTestTask.DAL
{
    public class Context : DbContext
    {
        /// <summary>
        /// Context
        /// </summary>
        public Context() : base("name=Chilindo")
        {            
            //Database.SetInitializer<Context>(new CreateDatabaseIfNotExists<Context>());      
        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Transaction> Transactions { get; set; }
 


        /// <summary>
        /// OnModelCreating
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .HasMany(e => e.Transactions)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transaction>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);
        }

    }
}
