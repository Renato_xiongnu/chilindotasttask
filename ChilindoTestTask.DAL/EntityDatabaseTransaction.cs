﻿using ChilindoTestTask.DAL.Interfaces;
using System.Data.Entity;

namespace ChilindoTestTask.DAL
{
    public class EntityDatabaseTransaction: IDatabaseTransaction
    {
        private DbContextTransaction _transaction;


        /// <summary>
        /// EntityDatabaseTransaction
        /// </summary>
        /// <param name="context"></param>
        public EntityDatabaseTransaction(DbContext context)
        {
            _transaction = context.Database.BeginTransaction();
        }


        /// <summary>
        /// Commit
        /// </summary>
        public void Commit()
        {
            _transaction.Commit();
        }


        /// <summary>
        /// Rollback
        /// </summary>
        public void Rollback()
        {
            _transaction.Rollback();
        }


        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            _transaction.Dispose();
        }


    }//class
}
