﻿using System;

namespace ChilindoTestTask.DAL.Interfaces
{
    public interface IDatabaseTransaction: IDisposable
    {
        /// <summary>
        /// Commit
        /// </summary>
        void Commit();


        /// <summary>
        /// Rollback
        /// </summary>
        void Rollback();

    }//interface
}
