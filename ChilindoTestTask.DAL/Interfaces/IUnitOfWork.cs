﻿using ChilindoTestTask.Model;
using System;


namespace ChilindoTestTask.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// AccountRepository
        /// </summary>
        IRepository<Account> AccountRepository { get; }


        /// <summary>
        /// TransRepository
        /// </summary>
        IRepository<Transaction> TransRepository { get; }



        /// <summary>
        /// BeginTransaction
        /// </summary>
        /// <returns></returns>
        IDatabaseTransaction BeginTransaction();


        /// <summary>
        /// Save
        /// </summary>
        void Save();
    }
}
