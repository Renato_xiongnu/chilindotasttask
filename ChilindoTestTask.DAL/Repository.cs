﻿using ChilindoTestTask.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace ChilindoTestTask.DAL
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public DbContext Context;
        public DbSet<T> DbSet;

        /// <summary>
        /// Repository
        /// </summary>
        /// <param name="ctx"></param>
        public Repository(DbContext ctx)
        {
            if (ctx == null)
            {
                Context = new Context();
            }
            else
            {
                this.Context = ctx;
            }

            DbSet = Context.Set<T>();
        }

        /// <summary>
        /// Repository
        /// </summary>
        public Repository()
        {
            Context = new Context();
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T GetById(int id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

  
        public IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable<T>();
        }

   
        public IQueryable<T> Filter(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 50)
        {
            int skipCount = index * size;
            var _resetSet = filter != null ? DbSet.Where(filter).AsQueryable() : DbSet.AsQueryable();
            _resetSet = skipCount == 0 ? _resetSet.Take(size) : _resetSet.Skip(skipCount).Take(size);
            total = _resetSet.Count();
            return _resetSet.AsQueryable();
        }


 


        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Insert(T entity)
        {
            DbSet.Add(entity);
            return Context.SaveChanges() > 0;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Update(T entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            return Context.SaveChanges() > 0;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Delete(T entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
            return Context.SaveChanges() > 0;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual bool Delete(int id)
        {
            var entity = DbSet.Find(id);
            return Context.SaveChanges() > 0;
        }

 


    }//class
}
