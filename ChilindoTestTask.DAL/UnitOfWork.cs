﻿using ChilindoTestTask.DAL.Interfaces;
using ChilindoTestTask.Model;
using System;


namespace ChilindoTestTask.DAL
{
    public partial class UnitOfWork : IUnitOfWork, IDisposable
    {
        private IRepository<Account> _accountRepository;

        private IRepository<Transaction> _transRepository;


        private Context _context = new Context();

        private bool disposed = false;



        /// <summary>
        /// AccountRepository
        /// </summary>
        public IRepository<Account> AccountRepository
        {
            get
            {
                if (_accountRepository == null)
                    _accountRepository = new Repository<Account>(_context);

                return _accountRepository;
            }
        }


        /// <summary>
        /// TransRepository
        /// </summary>
        public IRepository<Transaction> TransRepository
        {
            get
            {
                if (_transRepository == null)
                    _transRepository = new Repository<Transaction>(_context);

                return _transRepository;
            }
        }

 

        /// <summary>
        /// Save
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }


        /// <summary>
        /// BeginTransaction
        /// </summary>
        /// <returns></returns>
        public IDatabaseTransaction BeginTransaction()
        {
            return new EntityDatabaseTransaction(_context);
        }


        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }


        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }//class
}
