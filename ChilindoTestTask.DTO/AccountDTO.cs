﻿using System.Collections.Generic;
 

namespace ChilindoTestTask.DTO
{
    public class AccountDTO
    {
        public AccountDTO()
        {
            Transactions = new List<TransactionDTO>();
        }

        public int AccountId { get; set; }

        public int AccountNumber { get; set; }

        public IList<TransactionDTO> Transactions { get; set; }
    }
}
