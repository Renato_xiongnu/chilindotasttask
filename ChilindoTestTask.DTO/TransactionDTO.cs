﻿namespace ChilindoTestTask.DTO
{
    public class TransactionDTO
    {
        public int AccountId { get; set; }

        public int AccountNumber { get; set; }

        public decimal Amount { get; set; }
 
        public string Currency { get; set; }
    }
}
