namespace ChilindoTestTask.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;


    [Table("Transactions")]
    public partial class Transaction
    {
        public int Id { get; set; }

        public int AccountId { get; set; }

        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        [Required]
        [StringLength(3)]
        public string Currency { get; set; }

        public virtual Account Account { get; set; }
    }
}
