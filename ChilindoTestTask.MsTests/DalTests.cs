﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChilindoTestTask.DAL;
using ChilindoTestTask.DAL.Interfaces;
using System.Linq;
using ChilindoTestTask.Model;

namespace ChilindoTestTask.MsTests
{
    [TestClass]
    public class DalTests
    {
        private IUnitOfWork _service = null;

        private Account _acc;
        private Transaction _trans;

        [TestInitialize()]
        public void Initialize()
        {
            _service = new UnitOfWork();

            var rnd = new Random();


            _acc = new Account()
            {
                AccountNumber = rnd.Next(1, 100)
            };

        }


        [TestMethod]
        public void CreateAccountTest()
        {
            var accounts = _service.AccountRepository.GetAll().ToList();
            int count1 = accounts.Count;
            
            bool res = _service.AccountRepository.Insert(_acc);
            Assert.IsTrue(res);

            accounts = _service.AccountRepository.GetAll().ToList();
            int count2 = accounts.Count;

            Assert.IsTrue(count2 > count1);
        }


        [TestMethod]
        public void GetAccountsTest()
        {
            var countries = _service.AccountRepository.GetAll().ToList();
            Assert.IsTrue(countries.Count > 0);
        }


  


    }//class
}
