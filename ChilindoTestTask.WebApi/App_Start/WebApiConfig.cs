﻿using ChilindoTestTask.DAL;
using ChilindoTestTask.DAL.Interfaces;
using System.Web.Http;
using Microsoft.Practices.Unity;
 

namespace ChilindoTestTask.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            //json:
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            json.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;


   
            //unity
            IUnityContainer container = new UnityContainer();
            //container.RegisterType<ICatalogService, CatalogService>(new PerThreadLifetimeManager())
            //         .RegisterType<IDALContext, DALContext>();

            container.RegisterType<IUnitOfWork, UnitOfWork>(new PerThreadLifetimeManager());
            // .RegisterType<IDALContext, DALContext>()

            config.DependencyResolver = new UnityDependencyResolver(container);
        }


    }//class
}
