﻿using ChilindoTestTask.DAL.Interfaces;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using ChilindoTestTask.Model;
using ChilindoTestTask.WebApi.Controllers.API.Helper;
using ChilindoTestTask.WebApi.Filters;
using System.Data.Entity;
using ChilindoTestTask.DTO;
using System;
using System.Threading.Tasks;

namespace ChilindoTestTask.WebApi.Controllers.API
{
    public class TransactionController: ApiController
    {
        private IUnitOfWork _service;


        /// <summary>
        /// TransactionController
        /// </summary>
        /// <param name="srv"></param>
        public TransactionController(IUnitOfWork srv)
        {
            _service = srv;
        }


        /// <summary>
        /// Balance
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Balance/{accountNumber}")]
        public HttpResponseMessage Balance(int accountNumber)
        {
            if (accountNumber <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                var acc = _service.AccountRepository.Filter(p => p.AccountNumber == accountNumber).AsNoTracking().FirstOrDefault();

                //no such the account
                if (acc == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new ServerResponse
                    {
                        AccountNumber = accountNumber,
                        Currency = string.Empty,
                        IsSuccessful = false,
                        Message = "Such the account does not exist"
                    });
                }

                var trans = _service.TransRepository.Filter(p => p.Account.AccountNumber == accountNumber).ToList();
                decimal balance = trans.Select(p => p.Amount).Sum();


                return Request.CreateResponse(HttpStatusCode.OK, new ServerResponse
                {
                    AccountNumber = accountNumber,
                    Balance = balance,
                    Currency = trans.Count > 0 ? trans.First().Currency : string.Empty,
                    IsSuccessful = true,
                    Message = string.Empty
                });
            }
            catch  
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }



        /// <summary>
        /// Deposit
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        [HttpPut]
        [TransactionActionFilter("trans")]        
        [Route("api/Deposit")]
        public HttpResponseMessage Deposit([FromBody]TransactionDTO trans)
        {
            if (ModelState.IsValid )
            {
                try
                {
                    using (var dbTrans = _service.BeginTransaction())
                    {
                        var acc = _service.AccountRepository.Filter(p => p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();

                        if (acc == null)
                        {
                            var accNew = new Account
                            {
                                AccountNumber = trans.AccountNumber
                            };
                            bool accRes = _service.AccountRepository.Insert(accNew);

                            if (!accRes)
                            {
                                return Request.CreateResponse(HttpStatusCode.InternalServerError);
                            }
                            else
                            {
                                acc = _service.AccountRepository.Filter(p => p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();
                            }
                        }

                        var transModel = trans.ToTransactionModel();
                        transModel.AccountId = acc.AccountId;
                        bool res = _service.TransRepository.Insert(transModel);


                        decimal balance = _service.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();


                        dbTrans.Commit();

                        return Request.CreateResponse(HttpStatusCode.OK, new ServerResponse
                                                {
                                                    AccountNumber = acc.AccountNumber,
                                                    Balance = balance,
                                                    Currency = trans.Currency,
                                                    IsSuccessful = res,
                                                    Message = string.Empty
                                                });

                    }//dbTrans
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }



        /// <summary>
        /// Withdraw
        /// </summary>
        /// <param name="trans"></param>
        /// <returns></returns>
        [HttpPut]
        [TransactionActionFilter("trans")]
        [Route("api/Withdraw")]
        public HttpResponseMessage Withdraw(TransactionDTO trans)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var dbTrans = _service.BeginTransaction())
                    {
                        var acc = _service.AccountRepository.Filter(p => p.AccountNumber == trans.AccountNumber).AsNoTracking().FirstOrDefault();

                        //No such the account
                        if (acc == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, new ServerResponse
                                                        {
                                                            AccountNumber = acc.AccountNumber,
                                                            Currency = string.Empty,
                                                            IsSuccessful = false,
                                                            Message = "Such the account does not exist"
                                                        });
                        }

                        decimal balance = _service.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();

                        if (trans.Amount > balance)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, new ServerResponse
                                                        {
                                                            AccountNumber = acc.AccountNumber,
                                                            Currency = trans.Currency,
                                                            IsSuccessful = false,
                                                            Message = "Insufficient funds"
                                                        });
                        }

                        //trans is ok, add a new one with a negative sign
                        trans.Amount *= -1;
                        var transModel = trans.ToTransactionModel();
                        transModel.AccountId = acc.AccountId;
                        bool res = _service.TransRepository.Insert(transModel);


                        balance = _service.TransRepository.Filter(p => p.Account.AccountNumber == acc.AccountNumber).Select(p => p.Amount).Sum();

                        dbTrans.Commit();

                        return Request.CreateResponse(HttpStatusCode.OK, new ServerResponse
                                                        {
                                                            AccountNumber = acc.AccountNumber,
                                                            Balance = balance,
                                                            Currency = trans.Currency,
                                                            IsSuccessful = res,
                                                            Message = string.Empty
                                                        });

                    }//dbTrans
                }
                catch
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }


    }//class
}