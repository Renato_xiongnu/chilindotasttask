﻿using ChilindoTestTask.DTO;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ChilindoTestTask.WebApi.Controllers.API.Helper;

namespace ChilindoTestTask.WebApi.Filters
{
    public class TransactionActionFilter : ActionFilterAttribute
    {
        private readonly string _parameterName;


        /// <summary>
        /// TransactionActionFilter
        /// </summary>
        /// <param name="parameterName"></param>
        public TransactionActionFilter(string parameterName)
        {
            _parameterName = parameterName;
        }


        /// <summary>
        /// OnActionExecuting
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            foreach (var item in filterContext.ActionArguments)
            {
                if (item.Value is TransactionDTO)
                {
                    var obj = (TransactionDTO)item.Value;

                    if (!Verification.IsTransactionValid(obj))
                    {
                        filterContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                    }
                }
            }            
        }



    }//class
}