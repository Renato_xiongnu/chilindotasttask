The task represents a simple REST client which calls one of the following RESTful services:
---------------------------------------------------------------------------------------------
1) WCF Restful service
2) WebAPI Restful service

There are 3 operations:
--------------------------
- Balance: returns the balance of the given account
- Deposit: deposits the amount of the money to the account in the specified currency. In the case if account does not exist it creates a new one 
- Withdraw: withdraws the amount of the money from the account.
The response of the service represents the object with all the necessary incapsulated fields.


Implementation comments:
---------------------------
- There are 2 ways of RESTful service implementation in the project: WCF Rest and WebAPI. The second one is more advanced from the technology point of view
  But bear in mind that RESTful services is not a good approach when you work with the sensitive data, such as the banking accounts, etc.
- there is no check regarding the currency, so each account might have transactions with the different currencies
- currency type is nvarchar(3) and not nvarchar(2) as mentioned in the requirements, because international currency signs have 3 chars, e.g.: USD, EUR, TBT, etc.
- the signature of the rest wcf service public ServerResponse Balance(string accountNumber) takes accountNumber as of type string and not integer, because WebInvoke allows to use only strings.
In the case of using integer, we'd have to modify the URL: Balance/?accountNumber={accountNumber}
which is not a standard and will differ from the URL where we use WebAPI
accountNumber as of type integer is implemented in the WebAPI subproject.
- some part of the code within the Rest service could be moved into the BLL layer, but due to the small amount of the task's scope it does not make sense to do so.
- In the WebAPI, the resulting object is incapsulated into the HttpResponseMessage, however we could simply use just HttpResponseMessage without customer's ServerResponse object
- There is no try-catch in the WCF Rest service, it's done so to simplify the volume of work 
- The code of the Rest-client console app is not optimized because it serves only as a demo to make the rest calls
- There are only a few nUnit tests and only to cover some functionality of the DAL Layer